// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "ParserLib/parser_core/character_stream.h"
#include <iostream>
#include <fstream>
#include <streambuf>

using namespace std;
using namespace creaky::scripting;

int main(int, char**)
{
  ifstream t("../demos/DemoLineCounter.cpp");
  const string input(( istreambuf_iterator<char>(t) ), istreambuf_iterator<char>());

  const auto cs = new character_stream(input);
  character_provider* ics = cs;

  try
  {
    cout << "[Begin]" << endl;
    while (!ics->has_ended())
    {
      cout << ics->get_current();
      ics->advance();
    }
    cout << endl << "Total Lines: " << ics->where().row + 1 << endl;
  }
  catch (...)
  {
  }
  delete cs;

  return 0;
}