// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "ParserLib/parser_cpp/cpp_lexer.h"
#include <iostream>

using namespace std;
using namespace creaky::scripting;

int main(int, char**)
{
  creaky::scripting::cpp::lexer_for_cpp lex;

  for (;;)
  {
    string input;
    cout << "Type string to parse:" << endl;
    getline(cin, input);
    if (input.empty())
      break;

    lex.prepare(input);
    if (lex.execute())
    {
      for (;;)
      {
        auto t = lex.token_source()->pop();
        if (t.is_nothing() || t.is_end())
          break;

        cout << t.to_string() << endl;
      }
    }
  }

  return 0;
}