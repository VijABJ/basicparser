// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stack>
#include <string>
#include "ParserLib/parser_cpp/cpp_parser_types.hpp"
#include "ParserLib/parser_cpp/BasicParser.h"

using namespace std;
using namespace creaky::scripting;
using namespace creaky::scripting::cpp;

int nextTempNumber = 1;
const string TEMPORARY_NAME = "TEMP";
string GenerateTemporary()
{
  char buf[256];
  sprintf(buf, "%s%i", TEMPORARY_NAME.c_str(), nextTempNumber);
  nextTempNumber++;
  return string(buf);
}

bool IsLiteral(const parser_production_t t)
{
  const auto tt = t.type();
  return
    ( tt == +production_type_t::LITERAL_CHARACTER ) ||
    ( tt == +production_type_t::LITERAL_STRING ) ||
    ( tt == +production_type_t::LITERAL_BOOL ) ||
    ( tt == +production_type_t::LITERAL_INTEGER ) ||
    ( tt == +production_type_t::LITERAL_FLOAT )
    ;
}

int main(int, char**)
{
  using namespace basicparser;
  Parser parser;

  for (;;)
  {
    string input;
    cout << "Type string to parse:" << endl;
    getline(cin, input);
    if (input.empty())
      break;

    if (parser.parse(input))
    {
      cout << "Parsed successfully, interpreting..." << endl;

      stack<parser_production_t> yard;
      auto results = parser.get_results();

      while (!results.empty())
      {
        auto t = results.front();
        results.pop_front();

        switch (t.type())
        {
        case production_type_t::OPERATOR_PREFIX_UNARY:
        case production_type_t::OPERATOR_SUFFIX_UNARY:
        {
          auto operand = yard.top();
          yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, GenerateTemporary());

          cout << temp.value() << " = " << t.value() << " " << operand.value() << endl;
          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_ASSIGNMENT:
        case production_type_t::OPERATOR_ASSIGNMENT_EX:
        {
          auto rhs = yard.top(); yard.pop();
          auto lhs = yard.top(); yard.pop();
          auto temp = parser_production_t(lhs.type(), lhs.value() + "#");

          cout << temp.value() << " " << t.value() << " " << rhs.value() << endl;
          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_TERNARY:
        {
          auto ifFalse = yard.top(); yard.pop();
          auto ifTrue = yard.top(); yard.pop();
          auto valueToCheck = yard.top(); yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, valueToCheck.value() + "?");

          cout << temp.value() << " = Check( " << valueToCheck.value() <<
            " ), use IfTrue( " << ifTrue.value() <<
            " ), ifFalse( " << ifFalse.value() << " )" << endl;

          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_LOGICAL:
        case production_type_t::OPERATOR_RELATIONAL:
        case production_type_t::OPERATOR_ARITHMETIC:
        {
          auto operand1 = yard.top(); yard.pop();
          auto operand2 = yard.top(); yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, GenerateTemporary());

          cout << temp.value() << " = " << operand2.value() << " " << t.value() << " " << operand1.value() << endl;
          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_MEMBER:
        {
          auto child = yard.top(); yard.pop();
          auto parent = yard.top(); yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, GenerateTemporary() + "^");

          cout << temp.value() << " = Dereference member " << child.value() << " from " << parent.value() << endl;
          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_SCOPE:
        {
          auto child = yard.top(); yard.pop();
          auto parent = yard.top(); yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, GenerateTemporary() + "::");

          cout << temp.value() << " = referencing item " << child.value() << " from specified scope " << parent.value() << endl;
          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_TYPECAST:
          break;

        case production_type_t::OPERATOR_FUNCTION_CALL:
        {
          auto param = yard.top(); yard.pop();
          string parameter_list = "";
          while (param.type() != +production_type_t::META_ARGUMENTS_SENTINEL)
          {
            if (parameter_list.length() == 0)
            {
              parameter_list = param.value();
            }
            else
            {
              parameter_list = param.value() + ", " + parameter_list;
            }
            param = yard.top(); yard.pop();
          }

          auto functionId = yard.top(); yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, GenerateTemporary() + "!");
          cout << temp.value() << " = FUNCCALL " << functionId.value() << " with params: " << parameter_list << endl;
          yard.push(temp);
        }
        break;

        case production_type_t::OPERATOR_INDEX:
        {
          auto index = yard.top(); yard.pop();
          auto base = yard.top(); yard.pop();
          auto temp = parser_production_t(production_type_t::META_TEMPORARY, GenerateTemporary() + "[]");

          cout << temp.value() << " = AssignLValueOrValueOf => " <<
            index.value() << " index of " << base.value() << endl;

          yard.push(temp);
        }
        break;

        /// default goes to the yard
        default:
          yard.push(t);
          break;
        }
      }

      /// interpretation complete, value of top of stack....
      cout << "Final Resulting value in " << yard.top().value() << endl;
    }
    else
    {
      cout << "Error Encountered: " << parser.get_last_error() << endl;
    }

    cout << "=================================" << endl;
  }

  return 0;
}