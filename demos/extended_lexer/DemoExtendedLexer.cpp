// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include <iostream>
#include <fstream>
#include <streambuf>
#include "ParserLib/parser_cpp/cpp_lexer.h"

using namespace std;
using namespace creaky::scripting;

int main(int, char**)
{
  for (;;)
  {
    string filename;
    cout << "\n\nEnter filename: ";
    getline(cin, filename);
    if (filename.empty())
      break;

    /// might have to change this path and filename, again...
    ifstream fin(filename);
    string input(( istreambuf_iterator<char>(fin) ), istreambuf_iterator<char>());
    if (input.empty())
    {
      cout << endl << "FILE IS EMPTY, OR WASN'T FOUND." << endl;
      continue;
    }

    //auto cs = new lexer::character_stream(input);
    //lexer::character_provider* ics = cs;

    creaky::scripting::cpp::lexer_for_cpp lex;
    lex.prepare(input);

    for (;;)
    {
      auto t = lex.token_source()->pop();
      if (t.is_nothing() || t.is_end())
        break;

      cout << "Text= `" << t.lexeme() << "` ; Type=" << std::string(t.type()._to_string()) << endl;
    }
  }

  return 0;
}