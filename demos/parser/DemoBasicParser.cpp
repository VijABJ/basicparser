// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "../ParserLib/parser_cpp/BasicParser.h"
#include <iostream>

using namespace creaky::scripting;
using namespace creaky::scripting::cpp;

int main(int, char**)
{
  using namespace basicparser;
  Parser parser;

  for (;;)
  {
    std::string input;
    std::cout << "Type string to parse:" << std::endl;
    getline(std::cin, input);
    if (input.empty())
      break;

    if (parser.parse(input))
    {
      std::cout << "[Results]" << std::endl;
      auto result = parser.get_results();
      for (auto it = result.begin(); it != result.end(); ++it)
      {
        std::cout << "Type: " << std::string(it->type()._to_string()) << " Value: " << it->value() << std::endl;
      }
    }
    else
    {
      std::cout << "Error Encountered: " << parser.get_last_error() << std::endl;
    }

    std::cout << "=================================" << std::endl;
  }

  return 0;
}