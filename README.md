BasicParser ReadMe

[What is this]

This is a recursive-descent parser implementation that parses a C-like 
language based on the grammar as shown in http://www.nongnu.org/hcb/.
Not everything in that grammar are implemented mind, just the ones
that were needed at the time this library was written.  Feel free to 
expand, improve and fix stuff if you have the inclination.

[Prerequisites]

* C++ compiler - obviously
* CMake - for creating the VS solution files

[How to build]

Go in the builds folder (create one inside the project root if it's not there), 
and create a build folder.  You can also actually generate the build files in 
another folder, just DO NOT generate the project files inside the source folders!  

* VS2017 solution: > md VS2017, cd VS2017, cmake ../../. -G "Visual Studio 15 2017 Win64"
* unix makefile > md unix, cmake ../../.



[Projects Included]

* BasicParser - this is the library itself.  It has one class for lexical 
analysis, and another one for the actual parsing.  The lexer can be used
independent of the parser if you so desire.

* demo_lexer - exercises the lexer class by accepting input and spitting 
out a list tokens that were found.

* demo_parser - exercises the parser class.  Accepts statements, and breaks
it down into a postfix notation queue of tokens.

* demo_app -like demo_parser, but it outputs a pseudo-assembly language
output of the parsed tokens.