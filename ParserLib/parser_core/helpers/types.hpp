// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_HELPERS_TYPES_HPP)
#define CREAKY_HELPERS_TYPES_HPP

#include <vector>
#include <algorithm>
#include <string>

namespace creaky::helpers {

using string_list_t = std::vector<std::string>;

class unique_string_list
{
public:

  bool empty() const { return items_.empty(); }

  bool contains(const std::string& value) const
  {
    return std::find(items_.begin(), items_.end(), value) != items_.end();
  }

  bool add(const std::string& value)
  {
    const auto is_contained = contains(value);
    if (!is_contained)
      items_.push_back(value);

    return !is_contained;
  }

  bool add(const string_list_t& values)
  {
    auto added_count = 0;
    for(const auto& value : values)
    {
       if(!contains(value))
       {
         items_.push_back(value);
         added_count++;
       }
    }

    return added_count > 0;
  }

  void clear() { items_.clear(); }

private:
  string_list_t items_;
};

} /// creaky::helpers

#endif /// CREAKY_HELPERS_TYPES_HPP
