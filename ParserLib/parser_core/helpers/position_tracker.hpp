// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_HELPER_TRACKER_HPP)
#define CREAKY_SCRIPTING_HELPER_TRACKER_HPP

#include "mark_tracker.hpp"

namespace creaky::helpers {

class position_t
{
public:
  long position;
  long column;
  long row;

  position_t(const int p, const int c, const int r) :
    position(p), column(c), row(r)
  {}
  position_t() : position_t(0, 0, 0)
  {}

  position_t(const position_t& rhs) = default;
  position_t& operator=(const position_t& rhs) = default;
  position_t(position_t&& rhs) = default;
  position_t& operator=(position_t&& rhs) = default;

  void reset()
  {
    position = column = row = 0;
  };
};

class position_tracker : public mark_tracker<position_t>
{
public:
  virtual ~position_tracker() = default;

  const position_t& where() const { return where_; }

protected:
  position_t where_;

  position_t& create_mark() override
  {
    return where_;
  }
  void return_to_mark(const position_t& marker) override
  {
    where_ = marker;
  }
};

} /// creaky::helpers

#endif /// CREAKY_SCRIPTING_HELPER_TRACKER_HPP

