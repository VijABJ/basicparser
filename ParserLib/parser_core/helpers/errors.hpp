// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_HELPERS_ERRORS_HPP)
#define CREAKY_HELPERS_ERRORS_HPP

#include "types.hpp"
#include <memory>

namespace creaky::helpers{

class errors final
{
public:
  bool empty() const { return errors_.empty(); }
  bool is_raised() const { return !empty(); }

  const string_list_t& get() const { return errors_; }
  std::string get_composed() const
  {
    std::string s;
    for (const auto& item : errors_) s += item;
    return s;
  }

  std::string get_last() const
  {
    return errors_.empty() ? "" : errors_[errors_.size()-1];
  }

  bool raise(const std::string& message)
  {
    errors_.push_back(message);
    return false;
  };

  bool clear()
  {
    errors_.clear(); 
    return true;
  }

private:
  string_list_t errors_;
};

using errors_t = std::shared_ptr<errors>;

} /// creaky::helpers

#endif /// CREAKY_HELPERS_ERRORS_HPP
