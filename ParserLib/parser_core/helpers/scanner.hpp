// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_HELPERS_SCANNER_HPP)
#define CREAKY_SCRIPTING_HELPERS_SCANNER_HPP

namespace creaky::helpers {

template<typename T>
class scanner
{
public:
  virtual ~scanner() = default;

  virtual T get_current() = 0;
  virtual bool has_ended() const = 0;

  virtual bool restart() = 0;
  virtual bool advance() = 0;
  virtual bool retract() = 0;
};

} /// creaky::helpers

#endif /// CREAKY_SCRIPTING_HELPERS_SCANNER_HPP
