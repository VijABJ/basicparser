// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_HELPER_MARK_TRACKER_HPP)
#define CREAKY_SCRIPTING_HELPER_MARK_TRACKER_HPP

#include <stack>

namespace creaky::helpers {

template<typename T>
class mark_tracker
{
public:
  virtual ~mark_tracker() = default;

  void mark() 
  {
    marks_.push(create_mark());
  }

  void recall() 
  {
    if(!marks_.empty())
    {
      auto prev_marker = marks_.top();
      marks_.pop();
      return_to_mark(prev_marker);
    }
  }

  void unmark() 
  {
    if (!marks_.empty())
      marks_.pop();
  }

  void unmark_all()
  {
    while (!marks_.empty())
      marks_.pop();
  }

protected:
  using marker_list_t = std::stack<T>;
  marker_list_t marks_;

  virtual T& create_mark() = 0;
  virtual void return_to_mark(const T& marker) = 0;
};

} /// creaky::helpers

#endif /// CREAKY_SCRIPTING_HELPER_MARK_TRACKER_HPP
