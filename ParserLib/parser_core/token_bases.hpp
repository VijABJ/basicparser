// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_TOKEN_BASES_HPP)
#define CREAKY_SCRIPTING_TOKEN_BASES_HPP

#include <string>

namespace creaky::scripting {

class token_lexeme
{
public:
  explicit token_lexeme(const std::string& lexeme) : lexeme_(lexeme) {}

  const std::string& lexeme() const noexcept { return lexeme_; }

  // generic template value
  template<typename TResult> TResult as() { return TResult(); }

protected:
  std::string lexeme_;
};

/// float specializations
template<> inline double token_lexeme::as<double>() { return stod(lexeme_); }
template<> inline float token_lexeme::as<float>() { return stof(lexeme_); }

/// integers starting with 0 will be treated as octal
template<> inline int token_lexeme::as<int>() { return stoi(lexeme_, nullptr, lexeme_[0] == '0' ? 8 : 10); }
template<> inline long token_lexeme::as<long>() { return stol(lexeme_, nullptr, lexeme_[0] == '0' ? 8 : 10); }

/// add more cases here as needed.
template<> inline bool token_lexeme::as<bool>() { return (lexeme_ == "true") || (lexeme_ == "1"); }

template<typename T>
class token_base : public token_lexeme
{
public:
  token_base(const std::string& lexeme, const T type) : token_lexeme(lexeme), type_(type) {}
  virtual ~token_base() = default;

  T type() const noexcept { return type_; }

  virtual bool is_end() const noexcept = 0;
  virtual bool is_nothing() const noexcept = 0;
  virtual bool is_error() const noexcept = 0;
  virtual bool is_identifier() const noexcept = 0;
  virtual bool is_operator() const noexcept = 0;
  virtual bool is_literal() const noexcept = 0;

  virtual std::string to_string() const { return std::string(); }

protected:
  T type_;
};

} /// namespace creaky::scripting

#endif /// CREAKY_SCRIPTING_TOKEN_BASES_HPP

