// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CHARACTER_STREAM_H)
#define CREAKY_SCRIPTING_CHARACTER_STREAM_H

#include "character_provider.hpp"
#include <string>
#include <map>

namespace creaky::scripting {

class character_stream : public character_provider
{
public:
  explicit character_stream(const std::string& initial);
  character_stream() = delete;
  virtual ~character_stream();

  /// return the get_current character
  char get_current() override;

  /// returns true if the pointer points beyond all available data
  bool has_ended() const override;

  /// move the character pointer. returns false on overruns.
  bool restart() override;
  bool advance() override;
  bool retract() override;

private:
  std::string data_;
  std::map<long, long> line_lengths_;

  void record_line_length();
  void check_for_newline();
  void fix_lone_returns();
};

} /// namespace creaky::scripting

#endif /// CREAKY_SCRIPTING_CHARACTER_STREAM_H

