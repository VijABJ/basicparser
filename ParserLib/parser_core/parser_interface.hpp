// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_PARSER_INTERFACE_HPP)
#define CREAKY_SCRIPTING_PARSER_INTERFACE_HPP

#include "parser_token_provider.hpp"

namespace creaky::scripting {

template<typename TToken, typename TProduction>
class parser_interface
{
public:
  parser_interface() :
    errors_(std::make_shared<helpers::errors>()),
    producer_()
  { }
  virtual ~parser_interface() {}

  virtual bool parse(const std::string& input)
  {
    if (producer_.get() == nullptr)
      create_producer();

    if (!prepare(input))
      return false;

    return execute();
  }

  using parser_producer_t = std::shared_ptr<parser_producer_base<TToken>>;
  using productions_t = std::deque<production_t<TProduction>>;

  const productions_t& get_results()
  {
    return productions_;
  }

  std::string get_last_error() const
  {
    return errors_->get_composed();
  }

  virtual bool prepare(const std::string& input)
  {
    producer_->configure(errors_);
    producer_->prepare(input);
    return true;
  }
  virtual bool execute() = 0;

protected:
  helpers::errors_t errors_;
  parser_producer_t producer_;
  productions_t productions_;

  virtual void create_producer() = 0;
};

} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_PARSER_INTERFACE_HPP
