// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CHAR_PROVIDER_BASE_HPP)
#define CREAKY_SCRIPTING_CHAR_PROVIDER_BASE_HPP

#include "helpers/scanner.hpp"
#include "helpers/position_tracker.hpp"
#include "helpers/errors.hpp"
#include <memory>
#include <cctype>

namespace creaky::scripting {

class character_provider :
  public helpers::scanner<char>,
  public helpers::position_tracker
{
public:
  virtual ~character_provider() = default;
};

using character_provider_t = std::shared_ptr<character_provider>;

enum class visitor_result_t
{
  Nothing = 0,  /// nothing happened as the token scanner doesn't recognize its marker
  Ok,           /// found marker, token parsed successfully
  Error         /// found marker for token, but error happened. abort!
};

template<typename TToken>
class character_provider_visitor
{
public:
  virtual ~character_provider_visitor() = default;

  character_provider_visitor& configure(
    character_provider_t& source,
    helpers::errors_t& errors)
  {
    source_ = source;
    errors_ = errors;

    return *this;
  }

  /// scan the incoming stream pointed to by ics, and append the results
  /// to the tokens list.  return proper flag (see above).
  /// error/status messages goes into messages.
  virtual visitor_result_t scan(std::deque<TToken>& tokens) = 0;

protected:
  character_provider_t source_;
  helpers::errors_t errors_;

  virtual TToken create_token(const std::string&) { return TToken(); }

  bool is_valid() const
  {
    return (source_ != nullptr) && (errors_ != nullptr);
  }

  bool match(const std::string& match_this) const
  {
    auto c = source_->get_current();
    if (c != match_this[0])
      return false;

    source_->mark();
    source_->advance();

    for (size_t position = 1; position < match_this.length(); position++)
    {
      c = source_->get_current();
      // if there is no match, at any point in this loop, exit now
      // be sure to retract the incoming input though...
      if (c != match_this[position])
      {
        source_->recall();
        return false;
      }
      source_->advance();
    }

    return true;
  }

  std::string match_to_end_of_line() const
  {
    std::string result;
    for (;;)
    {
      if (source_->has_ended())
        break;

      const auto c = source_->get_current();
      if (is_end_of_line(c))
        break;

      result.append(1, c);
      source_->advance();
    }

    return result;
  }

  static bool is_end_of_line(const char c)
  {
    constexpr const auto string_linefeed = '\n';
    return c == string_linefeed;
  }

  static bool is_valid_id_char(const char c)
  {
    return isalnum(c) || (c == '_');
  }

  static bool is_valid_id_start(const char c)
  {
    return isalpha(c) || (c == '_');
  }

  static bool is_hex_digit(const char c)
  {
    return isdigit(c) || ((c >= 'a') && (c <= 'f')) || ((c >= 'A') && (c <= 'F'));
  }

  static bool is_octal_digit(const char c)
  {
    return (c >= '0') && (c <= '7');
  }

  static bool is_binary_digit(const char c)
  {
    return (c == '0') || (c == '1');
  }
};

} /// namespace creaky::scripting

#endif /// CREAKY_SCRIPTING_CHAR_PROVIDER_BASE_HPP

