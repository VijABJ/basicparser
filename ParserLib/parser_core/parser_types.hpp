// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_PARSER_TYPES_HPP)
#define CREAKY_SCRIPTING_PARSER_TYPES_HPP

#include "../3rdparty/enum.hpp"
#include <string>
#include <deque>
#include "token_bases.hpp"

namespace creaky::scripting {

BETTER_ENUM(literal_t, int, 
  Character,
  String,
  Integer,
  Float,
  Boolean
)

template<typename TProduction>
class production_t : public token_lexeme
{
public:
  production_t() : production_t(reinterpret_cast<TProduction>(0), "") {}
  explicit production_t(TProduction type, const std::string& value) : 
    token_lexeme(value), type_(type)
  {}

  TProduction type() const { return type_; }
  const std::string& value() const { return lexeme(); }

protected:
  TProduction type_;
};

} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_PARSER_TYPES_HPP
