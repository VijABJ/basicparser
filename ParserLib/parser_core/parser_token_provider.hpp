// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_PARSER_TOKEN_SOURCE_HPP)
#define CREAKY_SCRIPTING_PARSER_TOKEN_SOURCE_HPP

#include "parser_types.hpp"
#include "lexer_interface.hpp"
#include "helpers/errors.hpp"
#include <deque>

namespace creaky::scripting {

template<typename TToken>
class parser_producer_base
{
public:
  virtual ~parser_producer_base() = default;

  void configure(helpers::errors_t& errors)
  {
    errors_ = errors;
  }
  bool prepare(const std::string& input)
  {
    tokens_.clear();
    return run_lexer(input);
  }

  bool is_valid() const
  {
    return ((errors_ == nullptr) || (!errors_->is_raised()));
  }
  bool check_and_raise() const
  {
    if (is_valid() && tokens_.empty())
    {
      if (errors_ != nullptr)
        errors_->raise("Unexpected end of input.");

      return false;
    }

    return is_valid();
  }

  TToken peek()
  {
    if (!check_and_raise())
      return TToken();

    return tokens_.front();
  }
  TToken pop()
  {
    if (!check_and_raise())
      return TToken();

    auto t = tokens_.front();
    tokens_.pop_front();
    return t;
  }

  bool empty() const { return tokens_.empty(); }

  bool match_operator()
  {
    if (errors_->is_raised() || empty())
      return false;

    const auto t = peek();
    return (t.is_operator());
  }
  bool match_operator(const std::string& value)
  {
    if (errors_->is_raised() || empty())
      return false;

    auto t = peek();
    return (t.is_operator()) && (t.lexeme() == value);
  }
  bool match_operator(const std::vector<std::string>& values)
  {
    if (errors_->is_raised() || empty())
      return false;

    auto t = peek();
    if (!t.is_operator())
      return false;

    for (const auto& value : values)
    {
      if (value == t.lexeme())
        return true;
    }

    return false;
  }
  bool match_identifier()
  {
    if (errors_->is_raised() || empty())
      return false;

    auto t = peek();
    return (t.is_identifier());
  }
  bool match_identifier(const std::string& id_value)
  {
    if (errors_->is_raised() || empty())
      return false;

    auto t = peek();
    return (t.is_identifier()) && (t.lexeme() == id_value);
  }
  virtual bool match_literal(const literal_t literal) = 0;

protected:
  std::deque<TToken> tokens_;
  helpers::errors_t errors_;

  virtual bool run_lexer(const std::string& input) = 0;
};


} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_PARSER_TOKEN_SOURCE_HPP
