// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_SCANNER_FOR_COMMENTS_HPP)
#define CREAKY_SCRIPTING_SCANNER_FOR_COMMENTS_HPP

#include "../character_provider.hpp"

namespace creaky::scripting {

template<typename TToken>
class scanner_for_comment_single : public character_provider_visitor<TToken>
{
public:
  explicit scanner_for_comment_single(const std::string& prefix) : prefix_(prefix)
  {
    //if (prefix_.length() == 0)
    //  throw std::exception("prefix cannot be blank");
  }

  visitor_result_t scan(std::deque<TToken>& tokens) override
  {
    if (!this->match(prefix_))
      return visitor_result_t::Nothing;

    const auto lexeme(prefix_ + this->match_to_end_of_line());
    tokens.push_back(this->create_token(lexeme));

    return visitor_result_t::Ok;
  }

private:
  std::string prefix_;
};

template<typename TToken>
class scanner_for_comments : public character_provider_visitor<TToken>
{
public:
  scanner_for_comments(const std::string& opener, const std::string& closer) :
    opener_(opener), closer_(closer)
  {
    //if (opener_.length() == 0)
    //  throw std::exception("opener cannot be blank");
    //if (closer_.length() == 0)
    //  throw std::exception("closer cannot be blank");
  }
  visitor_result_t scan(std::deque<TToken>& tokens) override
  {
    if (!this->match(opener_))
      return visitor_result_t::Nothing;

    std::string lexeme;
    for (;;)
    {
      if (this->source_->has_ended())
      {
        this->errors_->raise("Unexpected end of input inside comments");
        return visitor_result_t::Error;
      }

      const auto c = this->source_->get_current();
      if (this->match(closer_))
        break;

      lexeme.append(1, c);
      this->source_->advance();
    }

    tokens.push_back(this->create_token(lexeme));
    return visitor_result_t::Ok;
  }

private:
  std::string opener_;
  std::string closer_;
};

} /// creaky::scriptiong

#endif /// CREAKY_SCRIPTING_SCANNER_FOR_COMMENTS_HPP