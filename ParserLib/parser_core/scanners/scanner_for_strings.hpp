// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_LEX_SCANNER_FOR_STRINGS_H)
#define CREAKY_SCRIPTING_LEX_SCANNER_FOR_STRINGS_H

#include "../character_provider.hpp"

namespace creaky::scripting {

template<typename TToken>
class scanner_for_string : public character_provider_visitor<TToken>
{
public:
  scanner_for_string() : quote_char_(0) {}

  visitor_result_t scan(std::deque<TToken>& tokens) override
  {
    auto c = this->source_->get_current();
    if ((c == '"') || (c == '\''))
    {
      quote_char_ = c;
      this->source_->advance();
      std::string lexeme{ "" };

      for (;;)
      {
        c = this->source_->get_current();

        // check for end of input, this is an error
        if (this->is_end_of_line(c))
        {
          this->errors_->raise("Unexpected end of input, parsing string literal");
          return visitor_result_t::Error;
        }

        // check for string termination, using the same quote we opened with
        if (c == quote_char_)
        {
          this->source_->advance();
          break;
        }

        // check for escape sequences, and do conversions if needed.
        if (c == '\\')
        {
          this->source_->advance();
          c = this->source_->get_current();
          switch (c)
          {
          case 'n':
            c = 10;
            break;
          case 'r':
            c = 13;
            break;
          case 't':
            c = 9;
            break;
          default:
            // do nothing, and accept this char as is
            break;
          }
        }

        // flow down here and append whatever is in c, to build our literal string
        this->source_->advance();
        lexeme.append(1, c);
      }

      // if we got here, we got the string literal parsed successfully
      tokens.push_back(this->create_token(lexeme));

      return visitor_result_t::Ok;
    }

    return visitor_result_t::Nothing;
  }

protected:
  char quote_char_;
};

} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_LEX_SCANNER_FOR_STRINGS_H
