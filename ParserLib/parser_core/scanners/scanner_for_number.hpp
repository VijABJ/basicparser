// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_LEX_SCANNER_FOR_NUMBER_H)
#define CREAKY_SCRIPTING_LEX_SCANNER_FOR_NUMBER_H

#include "../character_provider.hpp"

namespace creaky::scripting {

template<typename TToken>
class scanner_for_number : public character_provider_visitor<TToken>
{
public:
  visitor_result_t scan(std::deque<TToken>& tokens) override
  {
    const auto c = this->source_->get_current();
    if (isdigit(c))
    {
      std::string lexeme(1, c);
      this->source_->advance();

      auto tag_c = this->source_->get_current();
      if ((c == '0') && (tag_c == 'x'))
      {
        /// hex-formatted number
        lexeme.append(1, tag_c);
        this->source_->advance();

        /// check for another digit, we need at least '0xN'
        /// where 'N' is a valid hex digit.
        auto next_c = this->source_->get_current();
        if (!this->is_hex_digit(next_c))
        {
          /// format error detected
          this->errors_->raise("Malformed hexadecimal number detected.");
          return visitor_result_t::Error;
        }

        while (this->is_hex_digit(next_c))
        {
          lexeme.append(1, next_c);
          this->source_->advance();
          next_c = this->source_->get_current();
        }

        tokens.push_back(this->create_token(lexeme));

        return visitor_result_t::Ok;
      }
      else if ((c == '0') && (tag_c == 'b'))
      {
        /// binary-formatted number
        lexeme.append(1, tag_c);
        this->source_->advance();

        /// check for another digit, we need at least '0bN'
        /// where 'N' is a valid binary digit.
        auto next_c = this->source_->get_current();
        if (!this->is_binary_digit(next_c))
        {
          /// format error detected
          this->errors_->raise("Malformed binary number detected.");
          return visitor_result_t::Error;
        }

        while (this->is_binary_digit(next_c))
        {
          lexeme.append(1, next_c);
          this->source_->advance();
          next_c = this->source_->get_current();
        }

        tokens.push_back(this->create_token(lexeme));

        return visitor_result_t::Ok;
      }
      else
      {
        /// regular number
        while (isdigit(tag_c))
        {
          lexeme.append(1, tag_c);
          this->source_->advance();
          tag_c = this->source_->get_current();
        }

        /// check for a decimal point
        if (tag_c == '.')
        {
          /// continue floating point number parsing
          lexeme.append(1, tag_c);
          this->source_->advance();
          auto next_c = this->source_->get_current();

          if (!isdigit(next_c))
          {
            /// format error detected
            this->errors_->raise("Malformed floating point number detected.");
            return visitor_result_t::Error;
          }

          while (isdigit(next_c))
          {
            lexeme.append(1, next_c);
            this->source_->advance();
            next_c = this->source_->get_current();
          }

          tokens.push_back(this->create_token(lexeme));

          return visitor_result_t::Ok;
        }
        else
        {
          tokens.push_back(this->create_token(lexeme));

          return visitor_result_t::Ok;
        }
      }
    }
    return visitor_result_t::Nothing;
  }
};

} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_LEX_SCANNER_FOR_NUMBER_H