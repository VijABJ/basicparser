// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "scanner_for_operator.hpp"

creaky::scripting::detail::char_operator_node creaky::scripting::detail::char_operator_node::empty;

