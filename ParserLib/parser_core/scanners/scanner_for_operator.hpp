// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_LEX_SCANNER_FOR_OPERATOR_H)
#define CREAKY_SCRIPTING_LEX_SCANNER_FOR_OPERATOR_H

#include "../character_provider.hpp"
#include <vector>
#include <memory>
#include <algorithm>

namespace creaky::scripting {

namespace detail {

class char_operator_node;
using char_operator_node_t = std::shared_ptr<char_operator_node>;
using char_operators_t = std::vector<char_operator_node_t>;

class char_operators
{
public:
  char_operators() : characters_(), is_leaf_(false) {}
  virtual ~char_operators() = default;

  char_operators_t& characters() { return characters_; }

  bool exists(const char c) const
  {
    const auto it = std::find_if(characters_.begin(), characters_.end(),
      [&](auto item) { return item->character() == c; });

    return it != characters_.end();
  }

  char_operator_node* find(const char c);
  void insert(const std::string& op, const size_t position = 0);

  bool is_leaf() const { return is_leaf_; }

protected:
  char_operators_t characters_;
  bool is_leaf_;
};

class char_operator_node : public char_operators
{
public:
  explicit char_operator_node(const char c) : character_(c) {}
  char_operator_node() : char_operator_node('\0') {}
  virtual ~char_operator_node() = default;

  static char_operator_node empty;
  bool is_empty() const noexcept { return character_ == '\0'; }
  char character() const noexcept { return character_; }

protected:
  char character_;
};

inline char_operator_node* char_operators::find(const char c)
{
  const auto it = std::find_if(
    characters_.begin(), characters_.end(),
    [&](auto item) { return item->character() == c; });

  return (it != characters_.end()) ? it->get() : &char_operator_node::empty;
}
inline void char_operators::insert(const std::string& op, const size_t position)
{
  const auto c = op[position];

  auto item = find(c);
  if (item->is_empty())
  {
    characters_.push_back(std::make_shared<char_operator_node>(c));
    item = find(c);
  }

  if ((position + 1) >= op.length())
  {
    item->is_leaf_ = true;
    return;
  }

  item->insert(op, position + 1);
}

class recursive_operator_scanner : public char_operators
{
public:
  void add_operator(const std::string& op)
  {
    insert(op, 0);
  }
  bool has_operator(const char initial_character)
  {
    const auto item = find(initial_character);
    return !(item->is_empty());
  }
};

}

template<typename TToken>
class scanner_for_operator : public character_provider_visitor<TToken>
{
public:
  scanner_for_operator() : character_provider_visitor<TToken>(),
    impl_(std::make_shared<detail::recursive_operator_scanner>())
  {}
  visitor_result_t scan(std::deque<TToken>& tokens) override
  {
    auto c = this->source_->get_current();
    if (impl_->has_operator(c))
    {
      auto node = impl_->find(c);
      std::string lexeme(1, c);

      this->source_->mark();
      for (;;)
      {
        // move to next character, and get that one
        this->source_->advance();
        c = this->source_->get_current();

        const auto prev_was_leaf = node->is_leaf();
        node = node->find(c);

        if (node->is_empty())
        {
          if (prev_was_leaf)
          {
            // previous was a leaf, so let's push this token, as we're done
            tokens.push_back(this->create_token(lexeme));
            return visitor_result_t::Ok;
          }
          // not a leaf, so terminate this loop
          this->source_->recall();
          break;
        }

        // node is not empty, so let's keep going
        lexeme.push_back(c);
      }
    }

    return visitor_result_t::Nothing;
  }


  void add_operator(const std::string& op) const
  {
    impl_->add_operator(op);
  }
  void add_operators(const std::vector<std::string>& ops) const
  {
    for (auto& op : ops)
    {
      add_operator(op);
    }
  }

private:
  std::shared_ptr<detail::recursive_operator_scanner> impl_;
};

} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_LEX_SCANNER_FOR_OPERATOR_H