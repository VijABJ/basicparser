// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_LEX_SCANNER_FOR_IDENTIFIER_H)
#define CREAKY_SCRIPTING_LEX_SCANNER_FOR_IDENTIFIER_H

#include "../character_provider.hpp"

namespace creaky::scripting {

template<typename TToken>
class scanner_for_identifier : public character_provider_visitor<TToken>
{
public:
  visitor_result_t scan(std::deque<TToken>& tokens) override
  {
    const auto c = this->source_->get_current();
    if (this->is_valid_id_start(c))
    {
      std::string lexeme(1, c);
      this->source_->advance();

      auto next_c = this->source_->get_current();
      while (this->is_valid_id_char(next_c))
      {
        lexeme.append(1, next_c);
        this->source_->advance();
        next_c = this->source_->get_current();
      }

      tokens.push_back(this->create_token(lexeme));
      
      return visitor_result_t::Ok;
    }

    return visitor_result_t::Nothing;
  }
};

} /// creaky::scriptiong

#endif /// CREAKY_SCRIPTING_LEX_SCANNER_FOR_IDENTIFIER_H