// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "character_stream.h"

namespace cs = creaky::scripting;

constexpr const char STRING_RETURN = '\r';
constexpr const char STRING_LINEFEED = '\n';
constexpr const char STRING_TERMINATOR = '\0';
     
cs::character_stream::character_stream(const std::string& initial) :
  data_(initial)
{
}

cs::character_stream::~character_stream() = default;

char cs::character_stream::get_current() 
{
  return (where_.position >= (long)data_.length()) ? STRING_TERMINATOR : data_[where_.position];
}

bool cs::character_stream::has_ended() const
{
  return where_.position >= (long)data_.length();
}

bool cs::character_stream::restart()
{
  where_.reset();
  return true;
}

void cs::character_stream::record_line_length() 
{
  if ((data_[where_.position + 1] == STRING_RETURN) || (data_[where_.position + 1] == STRING_LINEFEED))
  {
    /// check if the get_current character is NOT an LF, store this
    /// <row, column> combo
    if (data_[where_.position] != STRING_LINEFEED)
    {
      line_lengths_.insert(std::make_pair(where_.row, where_.column));
    }
  }
}

void cs::character_stream::check_for_newline() 
{
  if (data_[where_.position] == STRING_LINEFEED)
  {
    where_.column = 0;
    where_.row++;
  }
  else
  {
    where_.column++;
  }
}

void cs::character_stream::fix_lone_returns() 
{
  /// fixup for when we're sitting on a CR, we don't want this
  /// be sure to also check we're not beyond our buffer!
  if ((where_.position < (long)data_.length()) && (data_[where_.position] == STRING_RETURN))
  {
    /// if the next char is LF, go there
    if (data_[where_.position + 1] == STRING_LINEFEED)
    {
      where_.position++;
    }
      /// not an LF, we have an abnormal input of just a single CR.
    /// normalize this and forced CR to be LF
    else
    {
      data_[where_.position] = STRING_LINEFEED;
    }
  }
}

bool cs::character_stream::advance()
{
  /// end of stream? return false right away
  if (where_.position >= (long)data_.length())
    return false;

  record_line_length();
  check_for_newline();

  /// now advance the buffer pointer
  where_.position++;

  fix_lone_returns();

  return true;
}

bool cs::character_stream::retract()
{
  if (where_.position <= 0)
    return false;

  where_.position--;
  /// if we hit a linefeed, back up one row, and restore column from dictionary
  if (data_[where_.position] == STRING_LINEFEED)
  {
    where_.row--;
    where_.column = line_lengths_[where_.row];
  }
  /// if this is a return, just retract the where_.position further back, change nothing else
  else if (data_[where_.position] == STRING_RETURN)
  {
    where_.position--;
  }
  /// otherwise, it's a plain ol' character, just back up one column
  else
  {
    where_.column--;
  }

  return true;
}




