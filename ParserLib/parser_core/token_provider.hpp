// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_TOKEN_PROVIDER_HPP)
#define CREAKY_SCRIPTING_TOKEN_PROVIDER_HPP

#include "helpers/scanner.hpp"
#include "helpers/errors.hpp"
#include "character_provider.hpp"
#include <vector>
#include <memory>
#include <stack>

namespace creaky::scripting {

template<typename TToken>
class token_provider : public helpers::scanner<TToken>
{
public:
  using collections_t = std::deque<TToken>;
  using token_stack_t = std::stack<TToken>;
  using visitor_t = character_provider_visitor<TToken>;
  using visitors_t = std::vector<std::shared_ptr<visitor_t>>;

  virtual ~token_provider() = default;

  const visitors_t& scanners() const { return scanners_; }


  void configure(character_provider_t& source, helpers::errors_t& errors)
  {
    source_ = source;
    errors_ = errors;
  }

  void add(visitor_t* visitor)
  {
    auto scanner = std::shared_ptr<visitor_t>(visitor);
    scanner->configure(source_, errors_);
    scanners_.push_back(scanner);
  }

  bool has_scanners() const
  {
    return !scanners_.empty();
  }

  void clear()
  {
    scanners_.clear();
  }

  TToken get_current() override
  {
    constexpr const size_t NEW_TOKENS_PER_DEMAND = 5;

    if (processed_.size() == 0)
    {
      for (auto i = NEW_TOKENS_PER_DEMAND; i > 0; i--)
      {
        if (!retrieve_token())
          break;
      }

      if (errors_->is_raised())
      {
        return TToken();
      }
    }

    return processed_.size() > 0 ?
      processed_.front() : TToken();
  }
  bool has_ended() const override
  {
    return processed_.empty();
  }
  bool restart() override
  {
    while (retract())
      ;

    return true;
  }
  bool advance() override
  {
    if (processed_.size() > 0)
    {
      const auto t = processed_.front();
      processed_.pop_front();
      unprocessed_.push(t);
    }

    return true;
  }
  bool retract() override
  {
    if (unprocessed_.size() > 0)
    {
      const auto t = unprocessed_.top();
      unprocessed_.pop();
      processed_.push_front(t);
    }

    return true;
  }

  TToken pop()
  {
    auto t = get_current();
    if (!t.is_end())
      advance();

    return t;
  };

  void retrieve_all() 
  {
    while (!source_->has_ended() && retrieve_token())
      ;

    skip_whitespaces();
  }

protected:
  helpers::errors_t errors_;
  character_provider_t source_;

  visitors_t scanners_;
  collections_t processed_;
  token_stack_t unprocessed_;

  void  skip_whitespaces() const
  {
    const std::string WHITESPACES(" \t\n\r");

    for (;;)
    {
      const auto c = source_->get_current();
      if (WHITESPACES.find(c) == std::string::npos)
        break;

      source_->advance();
    }
  }

  bool retrieve_token() 
  {
    skip_whitespaces();

    for (auto& ts : scanners_)
    {
      const auto result = ts->scan(processed_);

      if (result == visitor_result_t::Nothing)
        continue;

      if (result == visitor_result_t::Ok)
        return true;
    }

    return false;
  }
};

} /// creaky::scripting

#endif /// CREAKY_SCRIPTING_TOKEN_PROVIDER_HPP

