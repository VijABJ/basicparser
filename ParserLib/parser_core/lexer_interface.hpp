// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_LEXER_INTERFACE_HPP)
#define CREAKY_LEXER_INTERFACE_HPP

#include "helpers/errors.hpp"
#include "token_provider.hpp"
#include "character_stream.h"

namespace creaky::scripting {

template<typename TToken>
class lexer_base
{
public:
  using token_source_t = std::shared_ptr<token_provider<TToken>>;

  lexer_base() : source_(), 
    errors_(std::make_shared<helpers::errors>()),
    token_source_(std::make_shared<token_provider<TToken>>())
  {
  }
  virtual ~lexer_base() = default;

  helpers::errors_t& errors() { return errors_; }
  token_source_t& token_source() { return token_source_; }

  void prepare(const std::string& input) 
  {
    source_ = std::make_shared<character_stream>(input);
    token_source_->configure(source_, errors_);
    if(!token_source_->has_scanners())
      load_default_scanners();
  }
  bool execute() 
  {
    token_source_->retrieve_all();
    return source_->has_ended();    
  }
  virtual bool run(const std::string& input)
  {
    prepare(input);
    return execute();
  }

protected:
  character_provider_t source_;
  helpers::errors_t errors_;
  token_source_t token_source_;

  virtual void load_default_scanners() = 0;
};

} /// namespace creaky::scripting

#endif /// CREAKY_LEXER_INTERFACE_HPP

