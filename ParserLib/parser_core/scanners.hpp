// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_LEXER_SCANNERS_H)
#define CREAKY_SCRIPTING_LEXER_SCANNERS_H

#include "scanners/scanner_for_comments.hpp"
#include "scanners/scanner_for_strings.hpp"
#include "scanners/scanner_for_identifier.hpp"
#include "scanners/scanner_for_number.hpp"
#include "scanners/scanner_for_operator.hpp"


#endif /// CREAKY_SCRIPTING_LEXER_SCANNERS_H
