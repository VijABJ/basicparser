
cmake_minimum_required (VERSION 3.9)
project (parser_cpp)

file(GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.c*" "${CMAKE_CURRENT_SOURCE_DIR}/*.h*")

add_library(${PROJECT_NAME} ${SOURCES})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER libs)
target_link_libraries(${PROJECT_NAME} parser_core)

if(MSVC)
  set_target_properties(${PROJECT_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY ${LIB_DESTINATION})
  set_target_properties(${PROJECT_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${LIB_DESTINATION})
  set_target_properties(${PROJECT_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${LIB_DESTINATION})
endif()

