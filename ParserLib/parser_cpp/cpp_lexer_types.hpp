// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CPP_LEXER_TYPES_HPP)
#define CREAKY_SCRIPTING_CPP_LEXER_TYPES_HPP

#include "../../3rdparty/enum.hpp"

namespace creaky::scripting::cpp {

BETTER_ENUM(token_type, int, 
  Nothing = 0,
  Error,
  End,
  Identifier,
  DoubleQuotedString,
  SingleQuotedString,
  Integer,
  Float,
  Boolean,
  Operator,
  Comment
)

} /// creaky::scripting::cpp

#endif /// CREAKY_SCRIPTING_CPP_LEXER_TYPES_HPP
