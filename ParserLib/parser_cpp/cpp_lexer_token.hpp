// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CPP_LEXER_TOKEN_HPP)
#define CREAKY_SCRIPTING_CPP_LEXER_TOKEN_HPP

#include <string>
#include "cpp_lexer_types.hpp"
#include "../parser_core/token_bases.hpp"
#include "../parser_core/character_provider.hpp"
#include <memory>
#include <vector>
#include <string>

namespace creaky::scripting::cpp {

class token : public token_base<token_type>
{
public:
  token(const std::string& lexeme, const token_type type) : token_base(lexeme, type) {}
  token() : token("", token_type::Nothing) {}
  virtual ~token() = default;

  bool is_end() const noexcept override { return (+token_type::End == type_); }
  bool is_nothing() const noexcept  override { return (+token_type::Nothing == type_); }
  bool is_error() const noexcept override { return (+token_type::Error == type_); }

  bool is_identifier() const noexcept override { return (+token_type::Identifier == type_); }
  bool is_operator() const noexcept override { return (+token_type::Operator == type_); }
  bool is_literal() const noexcept override
  {
    return (+token_type::SingleQuotedString == type_)
      || (+token_type::DoubleQuotedString == type_)
      || (+token_type::Integer == type_)
      || (+token_type::Float == type_)
      || ((+token_type::Identifier == type_) && ((lexeme_ == "true") || (lexeme_ == "false")))
      ;
  }


  // quite inefficient. but this shouldn't be an issue as you 
  // shouldn't use this if you want speed. it's for display only.
  std::string to_string() const override
  {
    return "Token type = " + std::string(type_._to_string()) + ", lexeme = " + lexeme_;
  }
};

using tokens_collections_t = std::deque<token>;
using character_stream_visitor = character_provider_visitor<tokens_collections_t>;

using character_stream_visitor_t = std::shared_ptr<character_stream_visitor>;
using visitors_collection_t = std::vector<character_stream_visitor_t>;

} /// creaky::scripting::cpp

#endif /// CREAKY_SCRIPTING_CPP_LEXER_TOKEN_HPP

