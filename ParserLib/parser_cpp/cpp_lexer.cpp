// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "cpp_lexer.h"
#include "../parser_core/helpers/types.hpp"
#include "../parser_core/scanners.hpp"

namespace cs = creaky::scripting;
namespace helpers = creaky::helpers;
namespace csp = creaky::scripting::cpp;

helpers::string_list_t preprocessing_operators {
  "#", "##" 
};

helpers::string_list_t csp::lexer_for_cpp::unary_operators {
  "++", "--",
  "+", "-",
  "*", "&",
  "!", "~"
};
helpers::string_list_t binary_operators {
  "/", "%" 
};
helpers::string_list_t csp::lexer_for_cpp::assignment_operators {
  "=",
  "+=", "-=", "%=", "*=", "/=",
  "|=", "&=",  "^=", "<<=", ">>="
};
helpers::string_list_t relational_operators {
  "<", ">", "!=", "==", "<=", ">=", "&&", "||" 
};
helpers::string_list_t bit_operators {
  "|", "<<", ">>", "^" 
};
helpers::string_list_t delimiters {
  "(", ")", "{", "}", "[", "]", ";", "," 
};
helpers::string_list_t miscellaneous {
  "?", ".", "->", ":", "::"
};

class cpp_comment_single : public cs::scanner_for_comment_single<csp::token>
{
public:
  cpp_comment_single() : scanner_for_comment_single("//") {}

protected:
  csp::token create_token(const std::string& lexeme) override
  {
    return csp::token(lexeme, csp::token_type::Comment);
  }
};

class cpp_comments : public cs::scanner_for_comments<csp::token>
{
public:
  cpp_comments() : scanner_for_comments("/*", "*/") {}

protected:
  csp::token create_token(const std::string& lexeme) override
  {
    return csp::token(lexeme, csp::token_type::Comment);
  }
};

class cpp_string : public cs::scanner_for_string<csp::token>
{
protected:
  csp::token create_token(const std::string& lexeme) override
  {
    return csp::token(lexeme, 
      quote_char_ == '\'' ? 
        csp::token_type::SingleQuotedString : 
        csp::token_type::DoubleQuotedString);
  }
};

class cpp_identifier : public cs::scanner_for_identifier<csp::token>
{
protected:
  csp::token create_token(const std::string& lexeme) override
  {
    return csp::token(lexeme, csp::token_type::Identifier);
  }
};

class cpp_number : public cs::scanner_for_number<csp::token>
{
protected:
  csp::token create_token(const std::string& lexeme) override
  {
    return csp::token(lexeme, 
      lexeme.find('.') != std::string::npos ?
      csp::token_type::Float : csp::token_type::Integer);
  }
};

class cpp_operator : public cs::scanner_for_operator<csp::token>
{
protected:
  csp::token create_token(const std::string& lexeme) override
  {
    return csp::token(lexeme, csp::token_type::Operator);
  }
};

void csp::lexer_for_cpp::load_default_scanners()
{
  /// ORDER IS IMPORTANT!
  /// add special scanners first, especially the multi-character ones
  token_source_->add(new cpp_comment_single());
  token_source_->add(new cpp_comments());
  token_source_->add(new cpp_string());
  token_source_->add(new cpp_identifier());
  token_source_->add(new cpp_number());

  // add the operators last
  const auto ops = new cpp_operator();
  
  ops->add_operators(binary_operators);
  ops->add_operators(unary_operators);
  ops->add_operators(assignment_operators);
  ops->add_operators(relational_operators);
  ops->add_operators(bit_operators);
  ops->add_operators(delimiters);
  ops->add_operators(miscellaneous);

  token_source_->add(ops);
}


