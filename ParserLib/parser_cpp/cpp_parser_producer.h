// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CPP_PRODUCER_H)
#define CREAKY_SCRIPTING_CPP_PRODUCER_H

#include "../parser_core/parser_token_provider.hpp"
#include "cpp_lexer_token.hpp"

namespace creaky::scripting::cpp {

class cpp_parser_producer : public parser_producer_base<token>
{
public:
  virtual ~cpp_parser_producer() = default;

  bool match_literal(const literal_t literal) override;

protected:
  bool run_lexer(const std::string& input) override;
};

} /// creaky::scripting::cpp


#endif /// CREAKY_SCRIPTING_CPP_PRODUCER_H
