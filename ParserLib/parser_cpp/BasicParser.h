// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_PARSER_H)
#define CREAKY_SCRIPTING_PARSER_H

#include "cpp_parser.h"

namespace basicparser {

#define getAttribute type
#define getValue value
#define getResults get_results
#define ParseToken creaky::scripting::cpp::production_type_t

class Parser : public creaky::scripting::cpp::cpp_parser
{
};

} /// basicparser

#endif /// CREAKY_SCRIPTING_PARSER_H

