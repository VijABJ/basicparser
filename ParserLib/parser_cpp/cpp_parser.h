// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CPP_PARSER_H)
#define CREAKY_SCRIPTING_CPP_PARSER_H

#include "../parser_core/parser_interface.hpp"
#include "cpp_parser_producer.h"
#include "cpp_parser_types.hpp"

namespace creaky::scripting::cpp {

class cpp_parser : public parser_interface<token, production_type_t>
{
public:
  using production_type = production_t<production_type_t>;

  bool execute() override;

protected:
  void create_producer() override;

private:
  void generate(const production_type_t attribute, const std::string& value);

  std::string parseIdValue() const;
  void parsePrimaryExpression();
  void parseIdExpression();
  void parseIdExpressionAfterDot();
  void parseExpression();
  void parseExpressionList();
  void parsePostfixExpression();
  bool parsePostfixExpressionRest();

  void parseUnaryExpression();
  void parseCastExpression();
  void parseTypeId();

  void parseAssignmentExpression();
  void parseConditionalExpression();
  void parseLogicalOrExpression();
  void parseInitializerClause() const;

  void parseLogicalAndExpression();
  void parseInclusiveOrExpression();
  void parseExclusiveOrExpression();
  void parseAndExpression();
  void parseEqualityExpression();
  void parseRelationalExpression();
  void parseShiftExpression();

  void parseAdditiveExpression();
  void parseMultiplicativeExpression();
  void parsePmExpression();
};

} /// creaky::scripting::cpp

#endif /// CREAKY_SCRIPTING_CPP_PARSER_H

