// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "cpp_parser_producer.h"
#include "cpp_lexer.h"

bool creaky::scripting::cpp::cpp_parser_producer::match_literal(const literal_t literal)
{
  if (errors_->is_raised() || empty())
    return false;

  auto t = peek();
  switch (literal)
  {
  case literal_t::Character:
    return (t.type() == +token_type::SingleQuotedString) &&
      (t.lexeme().length() == 1);

  case literal_t::String:
    return (t.type() == +token_type::DoubleQuotedString);

  case literal_t::Integer:
    return (t.type() == +token_type::Integer);

  case literal_t::Float:
    return (t.type() == +token_type::Float);

  case literal_t::Boolean:
    return (t.type() == +token_type::Identifier) &&
      ((t.lexeme() == "true") || (t.lexeme() == "false"));

  default:
    return false;
  }
}

bool creaky::scripting::cpp::cpp_parser_producer::run_lexer(const std::string& input)
{
  auto lex = std::make_shared<lexer_for_cpp>();
  lex->prepare(input);

  if (!lex->execute())
  {
    if (errors_ != nullptr)
      errors_->raise("Lex Error: " + lex->errors()->get_composed());

    return false;
  }

  for (;;)
  {
    auto t = lex->token_source()->pop();

    if (t.is_nothing() || t.is_end())
      break;

    tokens_.push_back(t);
  }

  return true;
}