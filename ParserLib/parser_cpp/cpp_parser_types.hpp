// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CPP_PARSER_TYPES_HPP)
#define CREAKY_SCRIPTING_CPP_PARSER_TYPES_HPP

#include "../../3rdparty/enum.hpp"
#include "../parser_core/parser_types.hpp"

namespace creaky::scripting::cpp {

BETTER_ENUM(production_type_t, int,
  IDENTIFIER,
  LITERAL_CHARACTER,
  LITERAL_STRING,
  LITERAL_INTEGER,
  LITERAL_FLOAT,
  LITERAL_BOOL,

  OPERATOR_PREFIX_UNARY,
  OPERATOR_SUFFIX_UNARY,
  OPERATOR_ASSIGNMENT,
  OPERATOR_ASSIGNMENT_EX,
  OPERATOR_TERNARY,

  OPERATOR_LOGICAL,
  OPERATOR_RELATIONAL,
  OPERATOR_ARITHMETIC,
  OPERATOR_MEMBER,
  OPERATOR_SCOPE,

  OPERATOR_TYPECAST,
  OPERATOR_FUNCTION_CALL,
  OPERATOR_INDEX,

  META_EXPRESSION_SEPARATOR,
  META_ARGUMENTS_SENTINEL,
  META_TEMPORARY,

  NOTHING
)

using parser_production_t = production_t<production_type_t>;

} /// creaky::scripting::cpp

#endif /// CREAKY_SCRIPTING_CPP_PARSER_TYPES_HPP
