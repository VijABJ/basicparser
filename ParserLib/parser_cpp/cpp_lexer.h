// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#if !defined(CREAKY_SCRIPTING_CPP_LEXER)
#define CREAKY_SCRIPTING_CPP_LEXER

#include "../parser_core/lexer_interface.hpp"
#include "cpp_lexer_token.hpp"

namespace creaky::scripting::cpp {

class lexer_for_cpp : public lexer_base<cpp::token>
{
public:
  static helpers::string_list_t unary_operators;
  static helpers::string_list_t assignment_operators;

protected:
  void load_default_scanners() override;
};

} /// namespace creaky::scripting::lexer


#endif /// CREAKY_SCRIPTING_CPP_LEXER
