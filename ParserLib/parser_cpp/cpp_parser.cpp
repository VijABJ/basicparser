// This file is part of BasicParser, released under Boost Software License 1.0 (BSL-1.0).
// See docs/LICENSE.txt for license details, or visit https://opensource.org/licenses/bsl1.0
// See docs/COPYRIGHT.txt for copyright details.

#include "cpp_parser.h"
#include "cpp_lexer.h"
#include "cpp_parser_producer.h"

namespace csp = creaky::scripting::cpp;

void csp::cpp_parser::create_producer()
{
  producer_ = std::make_shared<cpp_parser_producer>();
}

bool csp::cpp_parser::execute()
{
  productions_.clear();
  parseExpression();

  return !errors_->is_raised();
}

void csp::cpp_parser::generate(const csp::production_type_t attribute, const std::string& value)
{
  productions_.push_back(production_type(attribute, value));
}

void csp::cpp_parser::parseExpression()
{
  if (errors_->is_raised())
    return;

  parseAssignmentExpression();
  if (producer_->match_operator(lexer_for_cpp::assignment_operators))
  {
    auto t = producer_->pop();
    const auto attrib =
      t.lexeme().length() == 1 ?
      production_type_t::OPERATOR_ASSIGNMENT : production_type_t::OPERATOR_ASSIGNMENT_EX;

    parseExpression();
    generate(attrib, t.lexeme());
  }

  while (producer_->match_operator(","))
  {
    producer_->pop();
    parseExpression();
  }
}


void csp::cpp_parser::parsePrimaryExpression()
{
  if (errors_->is_raised())
    return;

  auto t = producer_->peek();
  if (producer_->match_literal(literal_t::Character))
  {
    producer_->pop();
    generate(production_type_t::LITERAL_CHARACTER, t.lexeme());
  }
  else if (producer_->match_literal(literal_t::String))
  {
    producer_->pop();
    generate(production_type_t::LITERAL_STRING, t.lexeme());
  }
  else if (producer_->match_literal(literal_t::Integer))
  {
    producer_->pop();
    generate(production_type_t::LITERAL_INTEGER, t.lexeme());
  }
  else if (producer_->match_literal(literal_t::Float))
  {
    producer_->pop();
    generate(production_type_t::LITERAL_FLOAT, t.lexeme());
  }
  else if (producer_->match_literal(literal_t::Boolean))
  {
    producer_->pop();
    generate(production_type_t::LITERAL_BOOL, t.lexeme());
  }
  else if (producer_->match_operator("("))
  {
    producer_->pop();
    parseExpression();
    if (!producer_->match_operator(")"))
    {
      errors_->raise("Missing right parenthesis");
      return;
    }

    producer_->pop();
  }
  else
  {
    parseIdExpression();
  }
}

std::string csp::cpp_parser::parseIdValue() const
{
  if (errors_->is_raised())
    return "";

  if (!producer_->match_identifier())
  {
    errors_->raise("Identifier expected.");
    return "";
  }

  auto t = producer_->pop();
  return t.lexeme();
}

void csp::cpp_parser::parseIdExpression()
{
  if (errors_->is_raised())
    return;

  const auto idValue = parseIdValue();
  generate(production_type_t::IDENTIFIER, idValue);

  while (producer_->match_operator("::"))
  {
    auto op = producer_->pop();
    const auto id = parseIdValue();

    generate(production_type_t::IDENTIFIER, id);
    generate(production_type_t::OPERATOR_SCOPE, op.lexeme());
  }
}

void csp::cpp_parser::parseIdExpressionAfterDot()
{
  if (errors_->is_raised())
    return;

  const auto idValue = parseIdValue();
  generate(production_type_t::IDENTIFIER, idValue);
}

void csp::cpp_parser::parseExpressionList()
{
  if (errors_->is_raised())
    return;

  parseExpression();
  while (producer_->match_operator(","))
  {
    producer_->pop();
    parseExpression();
  }
}

void csp::cpp_parser::parsePostfixExpression()
{
  if (errors_->is_raised())
    return;

  parsePrimaryExpression();
  while (parsePostfixExpressionRest())
    ;
}

bool csp::cpp_parser::parsePostfixExpressionRest()
{
  if (errors_->is_raised())
    return false;

  if (producer_->match_operator("["))
  {
    producer_->pop();
    parseExpression();

    if (!producer_->match_operator("]"))
      return errors_->raise("Index operation expecting \"]\"");

    producer_->pop();
    generate(production_type_t::OPERATOR_INDEX, "[]");
    return true;
  }
  else if (producer_->match_operator("("))
  {
    producer_->pop();
    generate(production_type_t::META_ARGUMENTS_SENTINEL, "(");

    /// if parenthesis isn't next up, then parse for the arguments
    if (!producer_->match_operator(")"))
    {
      parseExpressionList();
      if (errors_->is_raised())
        return false;
    }

    /// if parenthesis STILL isn't next up, it's an error
    if (!producer_->match_operator(")"))
      return errors_->raise("Expecting close parenthesis");

    producer_->pop();
    generate(production_type_t::OPERATOR_FUNCTION_CALL, ")");
    return true;
  }
  else if (producer_->match_operator("."))
  {
    producer_->pop();
    parseIdExpressionAfterDot();
    if (errors_->is_raised())
      return false;

    generate(production_type_t::OPERATOR_MEMBER, ".");
    return true;
  }
  else if (producer_->match_operator("++") || producer_->match_operator("--"))
  {
    auto t = producer_->pop();
    generate(production_type_t::OPERATOR_SUFFIX_UNARY, t.lexeme());
    return true;
  }

  return false;
}

void csp::cpp_parser::parseUnaryExpression()
{
  if (errors_->is_raised())
    return;

  auto op = producer_->peek();
  if (producer_->match_operator(creaky::scripting::cpp::lexer_for_cpp::unary_operators))
  {
    producer_->pop();
    parseCastExpression();

    generate(production_type_t::OPERATOR_PREFIX_UNARY, op.lexeme());
  }
  else
  {
    parsePostfixExpression();
  }
}

void csp::cpp_parser::parseCastExpression()
{
  if (errors_->is_raised())
    return;

  /*if (producer_->match_operator("(")) {
      parseTypeId();
      /// cast associates right, possibly recursively, so it needs to be pushed
      /// to the output AFTER the value on its right.  to facilitate this, we'll
      /// just pop the one parseTypeId() pushed, and push it back in at the end of
      /// of the rest of this sequence
      auto pt = producer_->back();
      producer_->pop_back();
      /// now let's find the closing parenthesis
      if (!producer_->match_operator(")"))
          throw parse_error("Cast expression expecting \")\"");

      /// evaluate right side
      parseCastExpression();

      /// push back what we removed 2 steps ago
      producer_->push_back(pt);
  }
  else {*/
  parseUnaryExpression();
  //}
}

/// @TODO: this should only honor known types, but for our purposes, just
/// accept any valid identifier
void csp::cpp_parser::parseTypeId()
{
  if (errors_->is_raised())
    return;

  if (!producer_->match_identifier())
  {
    errors_->raise("Typeid expected");
    return;
  }

  auto op = producer_->pop();
  generate(production_type_t::OPERATOR_TYPECAST, op.lexeme());
}

void csp::cpp_parser::parseAssignmentExpression()
{
  if (errors_->is_raised())
    return;

  parseConditionalExpression();
}

void csp::cpp_parser::parseConditionalExpression()
{
  if (errors_->is_raised())
    return;

  parseLogicalOrExpression();
  if (producer_->match_operator("?"))
  {
    producer_->pop();

    parseExpression();
    if (!producer_->match_operator(":"))
    {
      errors_->raise("Ternary operator expecting \":\"");
      return;
    }

    producer_->pop();
    parseAssignmentExpression();

    generate(production_type_t::OPERATOR_TERNARY, "?:");
  }
}

void csp::cpp_parser::parseLogicalOrExpression()
{
  if (errors_->is_raised())
    return;

  parseLogicalAndExpression();
  while (producer_->match_operator("||"))
  {
    auto t = producer_->pop();

    parseLogicalAndExpression();
    generate(production_type_t::OPERATOR_LOGICAL, t.lexeme());
  }
}

void csp::cpp_parser::parseInitializerClause() const
{
  if (errors_->is_raised())
    return;
}

void csp::cpp_parser::parseLogicalAndExpression()
{
  if (errors_->is_raised())
    return;

  parseInclusiveOrExpression();
  while (producer_->match_operator("&&"))
  {
    auto t = producer_->pop();

    parseInclusiveOrExpression();
    generate(production_type_t::OPERATOR_LOGICAL, t.lexeme());
  }
}

void csp::cpp_parser::parseInclusiveOrExpression()
{
  if (errors_->is_raised())
    return;

  parseExclusiveOrExpression();
  while (producer_->match_operator("|"))
  {
    auto t = producer_->pop();

    parseExclusiveOrExpression();
    generate(production_type_t::OPERATOR_LOGICAL, t.lexeme());
  }
}

void csp::cpp_parser::parseExclusiveOrExpression()
{
  if (errors_->is_raised())
    return;

  parseAndExpression();
  if (producer_->match_operator("^"))
  {
    auto t = producer_->pop();

    parseAndExpression();
    generate(production_type_t::OPERATOR_LOGICAL, t.lexeme());
  }
}

void csp::cpp_parser::parseAndExpression()
{
  if (errors_->is_raised())
    return;

  parseEqualityExpression();
  while (producer_->match_operator("&"))
  {
    auto t = producer_->pop();

    parseEqualityExpression();
    generate(production_type_t::OPERATOR_LOGICAL, t.lexeme());
  }
}

void csp::cpp_parser::parseEqualityExpression()
{
  if (errors_->is_raised())
    return;

  parseRelationalExpression();
  if (producer_->match_operator("==") || producer_->match_operator("!="))
  {
    auto t = producer_->pop();

    parseRelationalExpression();
    generate(production_type_t::OPERATOR_RELATIONAL, t.lexeme());
  }
}

void csp::cpp_parser::parseRelationalExpression()
{
  if (errors_->is_raised())
    return;

  parseShiftExpression();
  if (producer_->match_operator("<") || producer_->match_operator("<=") || producer_->match_operator(">") || producer_->match_operator(">="))
  {
    auto t = producer_->pop();

    parseShiftExpression();
    generate(production_type_t::OPERATOR_RELATIONAL, t.lexeme());
  }
}

void csp::cpp_parser::parseShiftExpression()
{
  if (errors_->is_raised())
    return;

  parseAdditiveExpression();
  if (producer_->match_operator("<<") || producer_->match_operator(">>"))
  {
    auto t = producer_->pop();

    parseAdditiveExpression();
    generate(production_type_t::OPERATOR_LOGICAL, t.lexeme());
  }
}

void csp::cpp_parser::parseAdditiveExpression()
{
  if (errors_->is_raised())
    return;

  parseMultiplicativeExpression();
  while (producer_->match_operator("+") || producer_->match_operator("-"))
  {
    auto t = producer_->pop();

    parseMultiplicativeExpression();
    generate(production_type_t::OPERATOR_ARITHMETIC, t.lexeme());
  }
}

void csp::cpp_parser::parseMultiplicativeExpression()
{
  if (errors_->is_raised())
    return;

  parsePmExpression();
  while (producer_->match_operator("*") || producer_->match_operator("/") || producer_->match_operator("%") || producer_->match_operator("\\"))
  {
    auto t = producer_->pop();

    parsePmExpression();
    generate(production_type_t::OPERATOR_ARITHMETIC, t.lexeme());
  }
}

void csp::cpp_parser::parsePmExpression()
{
  if (errors_->is_raised())
    return;

  parseCastExpression();
  while (producer_->match_operator(".") || producer_->match_operator("->"))
  {
    auto t = producer_->pop();

    parseCastExpression();
    generate(production_type_t::OPERATOR_MEMBER, t.lexeme());
  }
}

